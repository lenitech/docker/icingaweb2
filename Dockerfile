ARG VERSION=latest
FROM icinga/icingaweb2:$VERSION
USER root
RUN mkdir -p /usr/share/icingaweb2/modules/netbox
COPY icingaweb2-module-netbox-v3.6.1.0.tar.gz /tmp/icingaweb2-module-netbox-v3.6.1.0.tar.gz
RUN tar xz -C /usr/share/icingaweb2/modules/netbox --strip 1 < /tmp/icingaweb2-module-netbox-v3.6.1.0.tar.gz
RUN rm /tmp/icingaweb2-module-netbox-v3.6.1.0.tar.gz
USER www-data
